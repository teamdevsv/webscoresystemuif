﻿<%@ Page Title="Verificar" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Verificar.aspx.vb" Inherits="WebScoreSystemUIF.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    
    <asp:Label ID="Label1" runat="server" Text="Dui"></asp:Label>
    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    <asp:Button ID="Button1" runat="server" Text="Verificar" />
    <br />
      <br />
      <br />
      <br />

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:BoundField DataField="primer_apellido" HeaderText="primer_apellido" SortExpression="primer_apellido" />
            <asp:BoundField DataField="segundo_apellido" HeaderText="segundo_apellido" SortExpression="segundo_apellido" />
            <asp:BoundField DataField="nombres" HeaderText="nombres" SortExpression="nombres" />
            <asp:BoundField DataField="genero" HeaderText="genero" SortExpression="genero" />
            <asp:BoundField DataField="lugar_de_nacimiento" HeaderText="lugar_de_nacimiento" SortExpression="lugar_de_nacimiento" />
            <asp:BoundField DataField="fecha_de_nacimiento" HeaderText="fecha_de_nacimiento" SortExpression="fecha_de_nacimiento" />
            <asp:BoundField DataField="direccion_domicilio" HeaderText="direccion_domicilio" SortExpression="direccion_domicilio" />
            <asp:BoundField DataField="pais" HeaderText="pais" SortExpression="pais" />
            <asp:BoundField DataField="ciudad" HeaderText="ciudad" SortExpression="ciudad" />
            <asp:BoundField DataField="departamento" HeaderText="departamento" SortExpression="departamento" />
            <asp:BoundField DataField="telefono_fijo" HeaderText="telefono_fijo" SortExpression="telefono_fijo" />
            <asp:BoundField DataField="telefono_celular" HeaderText="telefono_celular" SortExpression="telefono_celular" />
            <asp:BoundField DataField="correo_electronico" HeaderText="correo_electronico" SortExpression="correo_electronico" />
            <asp:BoundField DataField="numero_documento" HeaderText="numero_documento" SortExpression="numero_documento" />
            <asp:BoundField DataField="nombre_documento" HeaderText="nombre_documento" SortExpression="nombre_documento" />
            <asp:BoundField DataField="total_activo" HeaderText="total_activo" SortExpression="total_activo" />
            <asp:BoundField DataField="total_pasivo" HeaderText="total_pasivo" SortExpression="total_pasivo" />
            <asp:BoundField DataField="total_patrimonio" HeaderText="total_patrimonio" SortExpression="total_patrimonio" />
            <asp:BoundField DataField="ingresos_mensuales_prom" HeaderText="ingresos_mensuales_prom" SortExpression="ingresos_mensuales_prom" />
            <asp:BoundField DataField="egresos_mensuales_prom" HeaderText="egresos_mensuales_prom" SortExpression="egresos_mensuales_prom" />
            <asp:BoundField DataField="otros_ingreso_prom" HeaderText="otros_ingreso_prom" SortExpression="otros_ingreso_prom" />
            <asp:BoundField DataField="fuente_otros_ingresos" HeaderText="fuente_otros_ingresos" SortExpression="fuente_otros_ingresos" />
            <asp:BoundField DataField="actividades_internacionales" HeaderText="actividades_internacionales" SortExpression="actividades_internacionales" />
            <asp:CheckBoxField DataField="recursos_publicos" HeaderText="recursos_publicos" SortExpression="recursos_publicos" />
            <asp:CheckBoxField DataField="cargos_publicos" HeaderText="cargos_publicos" SortExpression="cargos_publicos" />
            <asp:CheckBoxField DataField="familiar_con_cargos_politicos" HeaderText="familiar_con_cargos_politicos" SortExpression="familiar_con_cargos_politicos" />
            <asp:BoundField DataField="cual_cargo_publico" HeaderText="cual_cargo_publico" SortExpression="cual_cargo_publico" />
            <asp:BoundField DataField="tipo_persona" HeaderText="tipo_persona" SortExpression="tipo_persona" />
            <asp:ButtonField Text="Aceptar" />
            <asp:ButtonField Text="Revision" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="datos" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:FormParameter FormField="num_doc" Name="num_doc" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <br />
    <br />
</asp:Content>

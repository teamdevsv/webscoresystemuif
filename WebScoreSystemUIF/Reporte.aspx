﻿<%@ Page Title="Reporte" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reporte.aspx.vb" Inherits="WebScoreSystemUIF.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   
     <h2><%: Title %></h2>

   <div class="container">
        <ul class="nav nav-tabs">
       <li class="active"><a data-toggle="tab" href="#SendEmail">Enviar Email</a></li>  
            <li><a data-toggle="tab" href="#Verificar">Verificar Contraparte</a></li>
            <li><a data-toggle="tab" href="#accountinformation">User Account Infromation</a></li>
        </ul>
        <div class="tab-content">
            <!-- Send Email -->
            <div id="SendEmail" class="tab-pane fade in active">
                <div class="jumbotron">

               
                <div class="form-group">
                    <div class="row">
                       
                            <div class="col-sm-2">
                                <span  >*  <asp:Label class="Star-clr" ID="Label1" runat="server" Text="Email:  "></asp:Label></span>
                              
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="Textenviar" runat="server" class="form-control mb-4" placeholder="E-mail"></asp:TextBox>
                               
                            </div>
                             <div class="col-sm-2" style="float: right">
                                
                                <asp:Button ID="Button1" runat="server" Text="Enviar"  class="btn btn-info btn-block my-4"/>
                            </div>
                        </div>
                        
                    </div>
                </div>
             </div>
               
               
          
            <!-- otra tab-->
            
            <div id="Verificar" class="tab-pane fade">

                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" >
                    <Columns>
                        <asp:BoundField DataField="primer_apellido" HeaderText="Primer Apellido" SortExpression="primer_apellido" />
                        <asp:BoundField DataField="segundo_apellido" HeaderText="Segundo Apellido" SortExpression="segundo_apellido" />
                        <asp:BoundField DataField="nombres" HeaderText="Nombres" SortExpression="nombres" />
                        <asp:BoundField DataField="genero" HeaderText="Genero" SortExpression="genero" />
                        <asp:BoundField DataField="numero_documento" HeaderText="Numero de Documento" SortExpression="numero_documento" />
                        <asp:BoundField DataField="fecha_de_nacimiento" HeaderText="Fecha de Nacimiento" SortExpression="fecha_de_nacimiento" />
                        <asp:BoundField DataField="lugar_de_nacimiento" HeaderText="Lugar de Nacimiento" SortExpression="lugar_de_nacimiento" />
                        <asp:BoundField DataField="pais" HeaderText="Pais" SortExpression="pais" />
                        <asp:BoundField DataField="ciudad" HeaderText="Ciudad" SortExpression="ciudad" />
                        <asp:BoundField DataField="departamento" HeaderText="Departamento" SortExpression="departamento" />
                        <asp:BoundField DataField="nombre_documento" HeaderText="Nombre Documento" SortExpression="nombre_documento" />
                        <asp:BoundField DataField="fecha_de_expedicion" HeaderText="Fecha de Expedicion" SortExpression="fecha_de_expedicion" />
                        <asp:BoundField DataField="lugar_de_expedicion" HeaderText="Lugar de Expedicion" SortExpression="lugar_de_expedicion" />
                        <asp:BoundField DataField="fecha_de_vencimiento" HeaderText="Fecha de Vencimiento" SortExpression="fecha_de_vencimiento" />
                        <asp:BoundField DataField="codigo_ticket" HeaderText="Codigo Ticket" ReadOnly="True" SortExpression="codigo_ticket" />
                        <asp:CheckBoxField DataField="Estado de verificacion" HeaderText="Estado de verificacion" SortExpression="Estado de verificacion" />
                        <asp:ButtonField   Text="Aceptar"   ButtonType="Button"  ControlStyle-CssClass="btn btn-info btn-block my-4"/>
                        <asp:ButtonField   Text="Revision" ButtonType="Button" ControlStyle-CssClass="btn btn-info btn-block my-4" />
                        
                    </Columns>
                </asp:GridView>


                

                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString2 %>" SelectCommand="DatosAVerificar" SelectCommandType="StoredProcedure"></asp:SqlDataSource>


                

            </div>
              <!-- otra tab-->
            <div id="accountinformation" class="tab-pane fade">

                <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">New Members</span>
                <span class="info-box-number">2,000</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            </div>
        </div>
    </div>
    
    
</asp:Content>
